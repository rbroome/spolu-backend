require 'spec_helper'

describe User do
   before { @user = User.new(image: "http://1.bp.blogspot.com/-kDNQy7tDriY/U6cpcnboZXI/AAAAAAAAI08/lWEmI_JQafQ/s1600/miss-tampa-bay-usa-seminar-weekend-1.png", 
                             latitude: '56', longitude: '11', gender: '0', looking_for_gender: '1', age: '26', distance: '25',
                             looking_for_upper_age: '30', looking_for_lower_age: '20') }
   before { @user2 = User.new(image: "http://1.bp.blogspot.com/-kDNQy7tDriY/U6cpcnboZXI/AAAAAAAAI08/lWEmI_JQafQ/s1600/miss-tampa-bay-usa-seminar-weekend-1.png", 
                             latitude: '56', longitude: '11', gender: '0', looking_for_gender: '1', age: '26', distance: '25',
                             looking_for_upper_age: '30', looking_for_lower_age: '20') }


   subject { @user }
   it { should respond_to(:matches)}

   it { should respond_to(:like_ids) }
   it { should respond_to(:passed_ids) }

   it { should be_valid }

   describe "image" do
      # using the accept_values_for gem
      it { should respond_to(:image) }
      it { should_not accept_values_for(:image, nil) } 
      it { should accept_values_for(:image, "a" * 2) }
      it { should accept_values_for(:image, "a" * 200) }
    end 
    describe "latitude" do
      it { should respond_to(:latitude) }
      it { should_not accept_values_for(:latitude, nil) }
      it { should_not accept_values_for(:latitude, 'hej') }
      it { should accept_values_for(:latitude, '12') }
      it { @user.latitude.should be_a(Float) }
    end
    describe "longitude" do
      it { should respond_to(:longitude) }
      it { should_not accept_values_for(:longitude, nil) }
      it { should_not accept_values_for(:longitude, 'hej') }
      it { should accept_values_for(:longitude, '12') }
      it { @user.longitude.should be_a(Float) }
    end
    describe "gender" do
      it { should respond_to(:gender) }
      it { should_not accept_values_for(:gender, nil) }
      it { should_not accept_values_for(:gender, 'hej') }
      it { should accept_values_for(:gender, '1') }
      it { @user.gender.should be_a(Fixnum) }
    end
    describe "looking_for_gender" do
      it { should respond_to(:looking_for_gender) }      
      it { should_not accept_values_for(:looking_for_gender, nil) }
      it { should_not accept_values_for(:looking_for_gender, 'hej') }
      it { should accept_values_for(:looking_for_gender, '1') }
      it { @user.looking_for_gender.should be_a(Fixnum) }
    end
    describe "age" do
      it { should respond_to(:age) }
      it { should_not accept_values_for(:age, nil) }
      it { should_not accept_values_for(:age, 'hej') }
      it { should accept_values_for(:age, '1') }
      it { @user.age.should be_a(Fixnum) }
    end
    describe "distance" do
      it { should respond_to(:distance) }
      it { should_not accept_values_for(:distance, nil) }
      it { should_not accept_values_for(:distance, 'hej') }
      it { should accept_values_for(:distance, '1') }
      it { @user.distance.should be_a(Fixnum) }
    end
    describe "looking_for_upper_age" do
      it { should respond_to(:looking_for_upper_age) }
      it { should_not accept_values_for(:looking_for_upper_age, nil) }
      it { should_not accept_values_for(:looking_for_upper_age, 'hej') }
      it { should accept_values_for(:looking_for_upper_age, '1') }
      it { @user.looking_for_upper_age.should be_a(Fixnum) }
    end
    describe "looking_for_lower_age" do
      it { should respond_to(:looking_for_lower_age) }
      it { should_not accept_values_for(:looking_for_lower_age, nil) }
      it { should_not accept_values_for(:looking_for_lower_age, 'hej') }
      it { should accept_values_for(:looking_for_lower_age, '1') }
      it { @user.looking_for_lower_age.should be_a(Fixnum) }
    end


end
