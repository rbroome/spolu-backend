class Pass < ActiveRecord::Base
	belongs_to :user, class_name: "User"
  	belongs_to :passed, class_name: "User"
  	validates :user_id, presence: true
  	validates :passed_id, presence: true
end
