class User < ActiveRecord::Base
	reverse_geocoded_by :latitude, :longitude
  	after_validation :reverse_geocode  # auto-fetch address

  	#validates :image, presence: true,:allow_nil => false
  	validates :longitude, numericality: {only_float: true},:allow_nil => false
  	validates :latitude, numericality: {only_float: true},:allow_nil => false
  	validates :gender, :numericality => true,:allow_nil => false
  	validates :looking_for_gender, :numericality => true,:allow_nil => false
  	validates :age, :numericality => true,:allow_nil => false
  	validates :distance, :numericality => true,:allow_nil => false
  	validates :looking_for_upper_age, :numericality => true,:allow_nil => false
  	validates :looking_for_lower_age, :numericality => true,:allow_nil => false



  	has_many :likes, foreign_key: "liker_id", dependent: :destroy
  	has_many :matches, foreign_key: "matcher_id", dependent: :destroy
  	has_many :passes, foreign_key: "user_id", dependent: :destroy
  	mount_uploader :image, ImageUploader

  # Follows a user.
  def like(other_user)
    likes.create!(liked_id: other_user.id)
  end

  # Returns true if the current user is following the other user.
  def likes?(other_user)
    !likes.find_by(liked_id: other_user.id).nil?
  end
  def liked_ids
  	likes.pluck(:liked_id)
  end
  def pass(other_user)
  	passes.create!(passed_id: other_user.id)
  end
  def passed_ids
  	passes.pluck(:passed_id)
  end

  def match_with(other_user)
  	matches.create!(matched_id: other_user.id)
  end
  def nearby
  	swiped_users = (self.liked_ids + self.passed_ids).uniq
  	swiped_users << -1 if swiped_users.empty?
  	self.nearbys(self.distance,{:order => 'updated_at'}).where("id NOT IN (?)",swiped_users).where(:gender => self.looking_for_gender,
  					  :age => self.looking_for_lower_age..self.looking_for_upper_age).limit(10)
  end

  	def User.users_matched_with(user)
   		following_ids = "SELECT matched_id FROM matches
    					 WHERE  matcher_id = :user_id"
    	where("id IN (#{following_ids})", user_id: user)
  	end

  	private 
  		def user_params
      	   params.require(:user).permit(:image, :latitude, :longitude,:gender,:looking_for_gender,:age,
      	   								:distance,:looking_for_upper_age,:looking_for_lower_age)
    	end
end



