json.array! @nearbys do |user|
	json.distance_km user.distance_to(@user,:km).round
	json._id user.id
	json.gender user.gender
	json.image user.image_url
	json.age user.age
end