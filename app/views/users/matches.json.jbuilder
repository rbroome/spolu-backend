json.array! @matches do |user|
    json.distance_km 5
    json._id user.id
    json.gender user.gender
    json.image user.image_url
    json.age user.age
end