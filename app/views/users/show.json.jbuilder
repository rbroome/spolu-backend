json._id @user.id
json.image @user.image_url
json.gender @user.gender
json.age @user.age
json.distance_km @user.distance_to(@user,:km).round
