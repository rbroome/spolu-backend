class UsersController < ApplicationController
	skip_before_filter :verify_authenticity_token  
	respond_to :json
  

  api :GET, '/users/:id'
  def show
    @user = User.find(params[:id])
  end

  api :POST,'/users'
  def create
    @user = User.new(user_params)

          #check if file is within picture_path
    if params[:picture_path]["file"]
      picture_path_params = params[:picture_path]
           #create a new tempfile named fileupload
      tempfile = Tempfile.new("fileupload")
      tempfile.binmode
           #get the file and decode it with base64 then write it to the tempfile
      tempfile.write(Base64.decode64(picture_path_params["file"]))
     
           #create a new uploaded file
      uploaded_file = ActionDispatch::Http::UploadedFile.new(:tempfile => tempfile, :filename => "profilepic.jpg", :original_filename => "profilepic.jpg") 
     
           #replace picture_path with the new uploaded file
      params[:picture_path] =  uploaded_file
      sleep 5
      end
    @user.image = params[:picture_path]

    if @user.save
      render json: {:_id => @user.id}
    else
      render json: {:errors => "failed to create user"}
    end
  end

  api :GET, '/nearby'
  def nearby
    @user = User.find(2)
    @nearbys = @user.nearby
  end
  api :GET, '/matches'
  def matches
    @user = User.find(2)
    @matches = User.users_matched_with(@user)
  end
  api :POST, '/like'
  def like
    @user = User.find(2)
    other_user = User.find(params[:id])
    @user.like(other_user)
    if other_user.likes?(@user)
      @user.match_with(other_user)
      other_user.match_with(@user)
      ## SHOULD ALSO SEND PUSH TO OTHER_USER!!!!!!
      push(other_user)
      render json: {:match => true}
    else
      ##not a match
      render json: {:match => false}
    end
  end
  api :POST, '/pass'
  def pass
    @user = User.find(2)
    other_user = User.find(params[:id])
    token = params[:token]
    
    @user.pass(other_user)
    render json: {:match => false}
  end

  def push(user)
    n = Rpush::Apns::Notification.new
    n.app = Rpush::Apns::App.find_by_name("ios_app")
    n.device_token = "7d9d778afa0aa5278e3cd8b485b4b6084bb96c89a7f0999fec5bba3c0cd2e983" #Marcus iphone..
    n.alert = "You got a new match"
    n.data = { id: user.id, push_Code: 1 }
    n.save!
  end

  private
    def user_params
      params.permit(:image,:latitude,:longitude,:gender,:looking_for_gender,:age,
                     :distance,:looking_for_upper_age,:looking_for_lower_age,:picture_path)
    end

end

