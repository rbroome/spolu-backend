class SessionsController < ApplicationController
	skip_before_filter :verify_authenticity_token  
	respond_to :json

	def create
		id = params["id"]
		if request.format != :json
		   render :status=>406, :json=>{:success => false, :message=>"The request must be json" }
		   return
		end
		if id.nil?
			#User not found, should create a new user
			return
		end
		@user = User.find(id)

		if @user.nil?
			render :status=>401, :json=>{:success => false,:message=> "Invalid email or password"}
			return
		end

		render :status=>200, :json=>{:success => true,
									 :message => "Logged in",
									 }
	end
end