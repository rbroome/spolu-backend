Rails.application.routes.draw do
  apipie
  resources :widgets

  resources :users
  match '/nearby', to: 'users#nearby', via: 'get'
  match '/matches', to: 'users#matches', via: 'get'
  match '/like', to: 'users#like', via: 'post'
  match '/pass', to: 'users#pass', via: 'post'
end



