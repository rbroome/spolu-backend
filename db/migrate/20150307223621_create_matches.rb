class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :matcher_id
      t.integer :matched_id

      t.timestamps
    end
  end
end
