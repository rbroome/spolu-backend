class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :image
      t.float :latitude
      t.float :longitude
      t.integer :gender
      t.integer :looking_for_gender
      t.integer :age
      t.integer :distance
      t.integer :looking_for_upper_age
      t.integer :looking_for_lower_age

      t.timestamps
    end
  end
end
