class CreatePasses < ActiveRecord::Migration
  def change
    create_table :passes do |t|
      t.integer :user_id
      t.integer :passed_id

      t.timestamps
    end
  end
end
